-- ATTENTION: RANDOM MUST BE SEEDED BEFORE USE!
-- There are three tables required. 
-- USERTAB: [UID]  = {}
-- 	-- uid: John Doe
-- 	-- emailA: name[@example.com]
-- 	-- emailB: [name@]example.com
-- 	-- active: boolean
-- AUTHTAB: [UID] = password
-- 	-- password: Salted SHA512 hash
-- SALTTAB: [UID] = SALT
-- salt: 256 character padding

-- It is recommended that these three tables be kept separate for data breaches.

--sha = require "support.sha2" -- DEBUG
forumman = forumman or {}

forumman.users={
	["add"] = function(uname, email, passwd, utab, authtab, salttab)
		if not uname or not email or not passwd then
			return false, "MISSING FIELDS"
		end
		utab = utab or forumman.instance.utab
		authtab = authtab or forumman.instance.authtab
		salttab = salttab or forumman.instance.salttab
		uid = math.floor(math.random()*(2^45))
		for try = 1, (forumman.config.failmax or 256) do
			if not utab[uid] then
				break
			end
		end
		if utab[uid] then
			return false, "FAILED TO GENERATE A RANDOM UID"
		end
		-- now we will make the user
		utab[uname] = {
			uid = uid,
			emaila = string.match(email,"(.-)@"),
			emailb = string.match(email,".-@(.+)"), 
			active=forumman.config.autoactivate or true
		}
		local function gensalt()
			local saltlength = math.random(16, 64)
			local salt = {}
			for i = 1, saltlength do
				salt[i] = string.char(math.random(0,255))
			end
			return table.concat(salt)
		end
		salttab[uid] = gensalt()
		-- Now salt the password. We place salt on front for now. We consider double salting.
		authtab[uid] = sha.sha512(salttab[uid]..passwd)
		
	end,
	["login"] = function(username, password, utab, authtab, salttab)
	utab = utab or forumman.instance.utab
	authtab = authtab or forumman.instance.authtab
	salttab = salttab or forumman.instance.salttab
		
		-- find that username.
		if not utab[username] then
			return forumman.users.email_login(username, password)
		end
		-- now get the uid
		local uid = utab[username].uid
		return ( sha.sha512(salttab[uid]..password) == authtab[uid] )
	end,	
	["email_login"] = function(email, password, utab, authtab, salttab)
		-- find that email. Ideally, we would have a reverse table, but for now, we find it manually.
		return false
	end,
}

-- PFAQ - Potentially frequently asked questions:
-- Q: Why do you generate a random UID, why not just use table.insert and ipairs?
	-- A: Paranoia. Should a 1334 h4ck3r get access to the table througha vulnerability, he won't be able to go 1,2,3,4 etc.
	-- It won't be the best protection from a black hat, but it might discourage a few script kiddies

