local function loadthread(topic)
	-- read through the file ignoring the first part
	local f, err = io.open(forumman.config.threadpath.."/"..forumman.threads[topic],"r")
	if err then return nil, err end
	local restab = {}
	local inheader = true
	local incontent = false
	local uid = ""
	local post = {}
	for l in f:lines() do
		if inheader then
			restab.uid, restab.date, restab.time = string.match(l, "Created by (%d+) on (.+) at (.+)")
print("DEBUG uid",restab.uid)
print("DEBUG date",restab.date)
print("DEBUG time",restab.time)
			inheader=false
		elseif incontent then
			if l == ".\r" then
				incontent = false
				table.insert(restab,{uid=uid, body=table.concat(post,"\n")})
			else
				table.insert(post, l)
print("DEBUG line",l)
			end
		else
			-- we are a uid
			uid = tonumber(l)
			incontent = true
print("DEBUG post uid",uid)
		end
	end
	return restab
end

local function savethread(topic, t)
	-- open the file for writing
	local f, err = io.open(forumman.config.threadpath.."/"..forumman.threads[topic],"w")
	if err then return nil, err end
	-- write the header:
	f:write(table.concat({"Created By ",t.uid, " on ",t.date," at ",t.time}))
	f:write("\r\n")
	for i, post in ipairs(t) do
		f:write(tostring(post.uid))
		f:write("\r\n")
		f:write(post.body)
		f:write("\r\n.\r\n")
	end
	f:close()
	return true
end

forumman.post = {
	["newthread"] = function(uid, topic)
		forumman.threads[topic] = os.date("%Y%m%d%H%M%S")
		f,err = io.open(forumman.config.threadpath.."/"..forumman.threads[topic],"w")
		if err then
			return nil, err
		end
		f:write("Created by ")
		f:write(tostring(uid))
		f:write(os.date(" on %A %B %d %Y at %H:%M"))
		f:write("\r\n")
	end,
	["newpost"] = function(uid, thread, content) -- returns pid
		if not forumman.threads[thread] then
			return nil, "NO SUCH THREAD"
		end
	--	table.insert(forumman.threads[thread].posts,{by=uid, body=content})
	-- 	open the file, and read it, then write uid and content
		f, err = io.open(forumman.config.threadpath.."/"..forumman.threads[thread], "r")
		if err then
			return nil, err
		end
		postlist = f:read("*all")
		f:close()
		f, err = io.open(forumman.config.threadpath.."/"..forumman.threads[thread], "w")
		if err then
			return nil, err
		end
		f:write(postlist)
		f:write(tostring(uid))
		f:write("\r\n") 
		f:write(content:gsub("\r\n%.\r\n","\n. \n"))
	--f:write(content)
		f:write("\r\n.\r\n")
		f:close()
	end,
	["deletepost"] = function(thread, pid)
		if not forumman.threads[thread] then
			return nil, "NO SUCH THREAD"
		end
-- NOTE for developers. This permantely deletes a post. consider backup options.
		t = loadthread(thread)
		t[pid] = nil
		savethread(thread, t)
	end,
	["updatepost"] = function(pid, newcontent)
		forumman.posts[pid].body=newcontent
	end,
	["getpost"] = function(pid)
		return forumman.posts[pid]
	end,
	["getthread"] = function(tid)
		return loadthread(tid)
	end,
	["deletethread"] = function(tid)
		-- delete all the posts
		os.execute("mkdir .trash")
		os.execute(table.concat({"mv ",forumman.config.threadpath,"/",forumman.threads[tid]," .trash"}))
		forumman.threads[tid] = nil
		-- TODO: garbage collection?
	end,
	["saveindex"] = function()
		f, err = io.open(forumman.config.threadpath.."index", "w")
		if err then
			print("ERROR: ", err)
			return nil, err
		end
		for title, id in pairs(forumman.threads) do
			f:write(id)
			f:write("\t")
			f:write(title)
			f:write("\n")
		end
		f:close()
	end,
	["savethread"] = function(topic, t)
		return savethread(topic, t)
	end
}
