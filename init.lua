forumman = {}
sha = sha or require "support.sha2"
require "config"
require "users"
require "post"

if forumman.config.threadpath then
	forumman.threads = {}
	f, err = io.open(forumman.config.threadpath.."index", "r")
	if err then
		if err:upper():match("DIRECTORY") then
			print("WARNING: CREATING DIRECTORY")
			os.execute("mkdir "..forumman.config.threadpath)
		else
			print("FORUMMAN ERROR", err)
		end
	else
		for l in f:lines() do
			-- each line consits of id ... title
			-- each entry in threads consists of title..id
			id, title = l:match("(%d+)\t(.*)")
			forumman.threads[title] = id

		end
		f:close()
	end
end
